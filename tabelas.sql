create database db_sudv;



create table tb_enfermeiros (
	id_enfermeiro  int NOT NULL UNIQUE ,
	nome_enfermeiro varchar(255) NOT NULL,
	nm_coren varchar(255) NOT NULL,
	login_enfermeiro varchar(255) NOT NULL,
	senha_enfermeiro varchar (255) NOT NULL,

	PRIMARY KEY (id_enfermeiro)

);


create table tb_pacientes (
	id_paciente  int NOT NULL UNIQUE ,
	nome_paciente varchar(255) NOT NULL,
	cpf_paciente varchar(14) NOT NULL,
	senha_paciente varchar (255) NOT NULL,
	dt_nascimento date,
	cns varchar (20) NOT NULL,
	nome_social varchar (255),
	genero_paciente varchar (2),
	cep_paciente varchar(10),
	rua_paciente varchar(255),
	numero_casa_paciente varchar (10),
	complemento_end varchar (255),
	bairro_paciente varchar (255),
	cidade_paciente varchar (255),
	estado_paciente varchar (255),
	telefone_paciente varchar (50),
	is_responsavel char,
	is_dependente char,

	PRIMARY KEY (id_paciente)

);

create table tb_dependente (
	id_dependente  int NOT NULL UNIQUE ,
	id_paciente int NOT NULL,
	PRIMARY KEY (id_dependente)
);

create table tb_responsavel (
	id_responsavel  int NOT NULL UNIQUE ,
	id_paciente int NOT NULL,
	PRIMARY KEY (id_responsavel)
);

create table tb_relacao_dep_resp (
	id_relacao_dep_resp  int NOT NULL UNIQUE ,
	id_dependente int NOT NULL,
	id_responsavel int NOT NULL ,
	PRIMARY KEY (id_relacao_dep_resp)
);

create table tb_vacina(
    id_vacina  int NOT NULL UNIQUE ,
    nome_vacina varchar (255) NOT NULL,
    lote varchar (255) NOT NULL,
    dt_validade date NOT NULL,
    fornecedor varchar(255) NOT NULL,
    nome_campanha varchar (255),
    idade_vacina varchar (255) NOT NULL,
    dose_vacina int NOT NULL,
    reforco_vacina varchar (255),
    aplicacao_vacina varchar (255) NOT NULL,

    PRIMARY KEY (id_vacina)
);

create table tb_carteiravacinacao (
	id_carteiravacinacao  int NOT NULL UNIQUE ,
	id_vacina int NOT NULL  ,
	id_paciente int NOT NULL,
	id_campanha int,
	id_enfermeira int NOT NULL,
	id_ubs int NOT NULL,
	data_vacinacao date,

	PRIMARY KEY (id_carteiravacinacao)

);

create table tb_ubs(
	id_ubs int NOT NULL UNIQUE ,
	nome_ubs varchar (255),
	cep_ubs varchar(10),
	rua_ubs varchar(255),
	numero_end_ubs varchar (10),
	complemento_end varchar (255),
	bairro_ubs varchar (255),
	cidade_ubs varchar (255),
	estado_ubs varchar (255),
	telefone_ubs varchar (50),
	email_ubs varchar (255),

	PRIMARY KEY (id_ubs)

);

create table tb_versao(
	nm_versao varchar (15)
);
ALTER TABLE `tb_carteiravacinacao` CHANGE `id_carteiravacinacao` `id_carteiravacinacao` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `tb_dependente` CHANGE `id_dependente` `id_dependente` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `tb_enfermeiros` CHANGE `id_enfermeiro` `id_enfermeiro` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `tb_pacientes` CHANGE `id_paciente` `id_paciente` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `tb_relacao_dep_resp` CHANGE `id_relacao_dep_resp` `id_relacao_dep_resp` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `tb_responsavel` CHANGE `id_responsavel` `id_responsavel` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `tb_ubs` CHANGE `id_ubs` `id_ubs` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `tb_vacina` CHANGE `id_vacina` `id_vacina` INT(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `tb_enfermeiros` ADD UNIQUE(`login_enfermeiro`);
ALTER TABLE `tb_pacientes` ADD UNIQUE(`cpf_paciente`);
ALTER TABLE `tb_pacientes` ADD UNIQUE(`cns`);
ALTER TABLE `tb_ubs` ADD UNIQUE(`email_ubs`);


ALTER TABLE  `tb_dependente` ADD FOREIGN KEY (`id_dependente`) REFERENCES `tb_dependente`(`id_dependente`) ON DELETE RESTRICT ON UPDATE RESTRICT; 
ALTER TABLE  `tb_dependente` ADD FOREIGN KEY (`id_paciente`) REFERENCES `tb_pacientes`(`id_paciente`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE  `tb_carteiravacinacao` ADD FOREIGN KEY (`id_enfermeira`) REFERENCES `tb_enfermeiros`(`id_enfermeiro`) ON DELETE RESTRICT ON UPDATE RESTRICT; 
ALTER TABLE  `tb_carteiravacinacao` ADD FOREIGN KEY (`id_paciente`) REFERENCES `tb_pacientes`(`id_paciente`) ON DELETE RESTRICT ON UPDATE RESTRICT; 
ALTER TABLE  `tb_carteiravacinacao` ADD FOREIGN KEY (`id_ubs`) REFERENCES `tb_ubs`(`id_ubs`) ON DELETE RESTRICT ON UPDATE RESTRICT; 
ALTER TABLE  `tb_carteiravacinacao` ADD FOREIGN KEY (`id_vacina`) REFERENCES `tb_vacina`(`id_vacina`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE  `tb_relacao_dep_resp` ADD FOREIGN KEY (`id_responsavel`) REFERENCES `tb_responsavel`(`id_responsavel`) ON DELETE RESTRICT ON UPDATE RESTRICT; 
ALTER TABLE  `tb_relacao_dep_resp` ADD FOREIGN KEY (`id_dependente`) REFERENCES `tb_dependente`(`id_dependente`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE  `tb_responsavel` ADD FOREIGN KEY (`id_paciente`) REFERENCES `tb_pacientes`(`id_paciente`) ON DELETE RESTRICT ON UPDATE RESTRICT;
