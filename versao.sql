
--v.0.0.2

ALTER TABLE `tb_pacientes` ADD `email_paciente` VARCHAR(255) NOT NULL AFTER `cpf_paciente`;
ALTER TABLE `tb_pacientes` ADD `cadastrado` CHAR(1) NOT NULL DEFAULT 'N' AFTER `is_dependente`;
ALTER TABLE `tb_pacientes` CHANGE `is_dependente` `is_dependente` CHAR(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'N';
ALTER TABLE `tb_pacientes` CHANGE `is_responsavel` `is_responsavel` CHAR(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'N';
ALTER TABLE `tb_pacientes` CHANGE `senha_paciente` `senha_paciente` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL;
ALTER TABLE `tb_pacientes` CHANGE `email_paciente` `email_paciente` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL;
UPDATE tb_pacientes SET senha_paciente = null;

INSERT INTO `tb_versao` (`nm_versao`) VALUES ('v.0.0.2');


--v.0.0.3
RENAME TABLE tb_carteiravacinacao TO tb_relacao_vacinacao;
ALTER TABLE `tb_relacao_vacinacao` CHANGE `id_carteiravacinacao` `id_relacao_vacinacao` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `db_sudv`.`tb_relacao_vacinacao` DROP INDEX `id_carteiravacinacao`, ADD UNIQUE `id_relacao_vacinacao` (`id_relacao_vacinacao`) USING BTREE;



--v.0.0.4

ALTER TABLE `tb_enfermeiros` ADD `ubs_id` INT(11) NOT NULL DEFAULT 1;
ALTER TABLE `tb_enfermeiros` ADD FOREIGN KEY (`id_ubs`) REFERENCES `tb_ubs`(`id_ubs`) ON DELETE RESTRICT ON UPDATE RESTRICT;
update `tb_versao` set nm_versao='v.0.0.3' WHERE nm_versao = 'v.0.0.4'

--v.0.0.4
ALTER TABLE tb_relacao_dep_resp DROP FOREIGN KEY tb_relacao_dep_resp_ibfk_1; 

ALTER TABLE tb_relacao_dep_resp ADD CONSTRAINT tb_relacao_dep_resp_ibfk_1 FOREIGN KEY (id_responsavel) 
REFERENCES tb_pacientes(id_paciente) ON DELETE RESTRICT ON UPDATE RESTRICT; 

ALTER TABLE tb_relacao_dep_resp DROP FOREIGN KEY tb_relacao_dep_resp_ibfk_2; ALTER TABLE tb_relacao_dep_resp 
ADD CONSTRAINT tb_relacao_dep_resp_ibfk_2 FOREIGN KEY (id_dependente) REFERENCES tb_pacientes(id_paciente) 
ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE db_sudv.tb_responsavel DROP FOREIGN KEY tb_responsavel_ibfk_1;
ALTER TABLE db_sudv.tb_dependente DROP FOREIGN KEY tb_dependente_ibfk_2
ALTER TABLE db_sudv.tb_dependente DROP FOREIGN KEY tb_dependente_ibfk_1

CREATE TABLE db_sudv.tb_relacao_dep_resp ( 
    id_dependente INT NOT NULL , 
    id_relacao_dep_resp INT NOT NULL AUTO_INCREMENT , 
    id_responsavel INT NOT NULL , PRIMARY KEY (id_relacao_dep_resp)) ENGINE = InnoDB;

ALTER TABLE tb_relacao_dep_resp ADD FOREIGN KEY (id_dependente) REFERENCES tb_pacientes(id_paciente) 
ON DELETE RESTRICT ON UPDATE RESTRICT; 

ALTER TABLE tb_relacao_dep_resp ADD FOREIGN KEY (id_responsavel) REFERENCES tb_pacientes(id_paciente) 
ON DELETE RESTRICT ON UPDATE RESTRICT;

INSERT INTO `tb_versao` (`nm_versao`) VALUES ('v.0.0.4');



--v.0.0.5
ALTER TABLE `tb_vacina` ADD `cor` VARCHAR(50) NOT NULL DEFAULT '#F8ECE0' AFTER `aplicacao_vacina`;
ALTER TABLE `tb_vacina` ADD `ativa` int NOT NULL DEFAULT 1;
ALTER TABLE `tb_vacina` ADD `id_ubs` int NOT NULL DEFAULT 1 ;
ALTER TABLE `tb_vacina` ADD FOREIGN KEY (`id_ubs`) REFERENCES `tb_ubs`(`id_ubs`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `tb_enfermeiro` ADD `id_ubs` int NOT NULL DEFAULT 1 ;
ALTER TABLE `tb_enfermeiro` ADD FOREIGN KEY (`id_ubs`) REFERENCES `tb_ubs`(`id_ubs`) ON DELETE RESTRICT ON UPDATE RESTRICT;
update `tb_versao` set nm_versao='v.0.0.3' WHERE nm_versao = 'v.0.0.5'

